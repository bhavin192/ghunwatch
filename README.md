# ghunwatch

[![Go Report
Card](https://goreportcard.com/badge/gitlab.com/bhavin192/ghunwatch)](https://goreportcard.com/report/gitlab.com/bhavin192/ghunwatch)

ghunwatch deletes all the repository subscriptions for given
organization on GitHub. (Unwatch/Unsubscribe all repositories from the
organization).  Uses GitHub APIs to achieve this task.

- [GitHub Developer Guide: Repositories: List organization
  repositories](https://developer.github.com/v3/repos/#list-organization-repositories)
- [GitHub Developer Guide: Watching: Delete a Repository
Subscription](https://developer.github.com/v3/activity/watching/#delete-a-repository-subscription)

## How to use
- Make sure you have `go` installed on your machine
- Install ghunwatch with go get
  ```sh
  $ go get -u gitlab.com/bhavin192/ghunwatch
  ```
- ghunwatch should be available to use as
  ```sh
  $ ghunwatch --help
  Usage of ./ghunwatch:
	-org string
		  Name of the organization to fetch repos from
  # Example: unwatch all repos from NonExistingOrg
  $ ghunwatch -org NonExistingOrg
  ```

  This will ask username and token. *Make sure the token has `repo`
  and `notifications` scope.*

## Licensing

ghunwatch is licensed under GNU General Public License v3.0. See
[LICENSE](./LICENSE) for the full license text.
