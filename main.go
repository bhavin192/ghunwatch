// ghunwatch removes watches (unwatch/unsubscribe) from all the
// repositories of given organization
package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/tomnomnom/linkheader"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
)

const (
	apiEndPoint = "https://api.github.com"
	repoSubURL  = "/repos/%s/subscription"
	orgReposURL = "/orgs/%s/repos"
)

type repo struct {
	FullName string `json:"full_name"`
}

func main() {
	var orgName = flag.String("org", "", "Name of the organization to fetch repositories from")
	flag.Parse()

	if *orgName == "" {
		log.Fatal("Organization name cannot be empty. See --help for more information.")
	}
	// RandomStuffs
	u, p := getBasicAuth()
	fmt.Printf("Fetching list of repositories under '%s', this may take some time.\n", *orgName)
	repos, err := getOrgRepoList(*orgName, u, p)
	if err != nil {
		log.Fatalf("error getting list of repositories for '%s': %v", *orgName, err)
	}
	fmt.Printf("/!\\ Deleting watches from %d repositories.\n", len(repos))
	fmt.Printf("Unwatched %d repositories.\n", unWatchRepos(repos, u, p))
}

// unWatchrepos takes a list of repo objects, username and
// password. Returns the number of repositories successfully
// unwatched. Logs error messages to Stdout if any. This does 20
// concurrent requests to GitHub API at a time.
func unWatchRepos(repos []repo, user, pass string) int {
	var c int
	var mu sync.Mutex
	var rc sync.WaitGroup
	// tokens is a counting semaphore used to
	// enforce a limit of 20 concurrent requests.
	var tokens = make(chan struct{}, 20)

	for _, r := range repos {
		rc.Add(1)
		go func(r repo) {
			tokens <- struct{}{}
			defer func() {
				<-tokens
				rc.Done()
			}()

			url := fmt.Sprintf(apiEndPoint+repoSubURL, r.FullName)
			req, err := http.NewRequest(http.MethodDelete, url, nil)
			if err != nil {
				log.Printf("error creating reqest for %s: %v", url, err)
			}
			req.SetBasicAuth(user, pass)

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Printf("request failed for %s: %v", url, err)
			}
			if resp.StatusCode != http.StatusNoContent {
				log.Printf("failed to unwatch repo %s: %s.", r.FullName, resp.Status)
				bodyByte, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Printf("error reading response body: %v", err)
				}
				log.Printf("response from server: %s", bodyByte)
			} else {
				mu.Lock()
				c++
				mu.Unlock()
				log.Printf("successfully unwatched %s", r.FullName)
			}
		}(r)
	}

	// Wait for all the requests to finish
	rc.Wait()

	return c
}

// getBasicAuth asks user to enter username and password/token and
// returns as string. Token users can keep the username field blank.
func getBasicAuth() (username, token string) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Enter username:")
	scanner.Scan()
	username = scanner.Text()
	// TODO (bhavin192): use golang.org/x/crypto/ssh/terminal to
	// read password from terminal
	fmt.Println("Enter token:")
	scanner.Scan()
	token = scanner.Text()
	return username, token
}

// getOrgRepoList returns the list of repo objects for given
// organization name and error if any. Follows all the Link headers
// with rel=next to collect entire list from all pages.
// ref: https://developer.github.com/v3/#pagination
func getOrgRepoList(name, user, pass string) ([]repo, error) {
	var repos []repo
	url := fmt.Sprintf(apiEndPoint+orgReposURL, name)
loop:
	for {
		req, err := http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			return repos, fmt.Errorf("error creating request object for %s: %v", url, err)
		}
		req.SetBasicAuth(user, pass)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return repos, fmt.Errorf("request failed for %s: %v", url, err)
		}
		bodyByte, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return repos, fmt.Errorf("error reading response body: %v", err)
		}
		if resp.StatusCode != http.StatusOK {
			return repos, fmt.Errorf("failed to get repository list for %s: %s. %s", name, resp.Status, bodyByte)
		}
		var rs []repo
		if err = json.Unmarshal(bodyByte, &rs); err != nil {
			return repos, fmt.Errorf("error converting response to object %s: %v", bodyByte, err)
		}
		repos = append(repos, rs...)

		links := linkheader.Parse(resp.Header.Get("Link"))
		for _, l := range links {
			if l.Rel == "next" {
				url = l.URL
				continue loop
			}
		}
		return repos, nil
	}
}
